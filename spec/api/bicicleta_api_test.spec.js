var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

;var base_url = 'http://localhost:3000/api/bicicletas'

//beforeEach(() => {Bicicleta.allBicis = [];});

describe('Bicicleta API', () =>{
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test base de datos');
            done();
        })
    });

    afterEach(function (done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /',() =>{
        it('Status 200', (done) =>{
            request.get(base_url, function(err, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        })
    });

    describe('POST BICICLETAS /create',() =>{
        it('Status 200', (done) =>{
            request.get(base_url, function(err, response, body){
                var headers = {'content-type' : 'application/json'};
                var aBici = {"code":10, "color":"rojo", "modelo":"urbana", "lat":-34, "lng":-54};
                request.post({
                    headers : headers,
                    url : base_url + '/create',
                    body : aBici
                }, function(err, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicletas;
                    console.log(bici);
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-54);
                    done();
                });
            });
        })
    });

});

/*describe('POST BICICLETAS /create', () =>{
    it('Status 200', (done) => {
        var headers= {'content-type': 'application/json'};
        var aBici='{"id":10, "color":"rojo", "modelo":"urbana","lat": -51, "lng": -51}';
        request.post({
            headers:headers,
            url : 'http://localhost:3000/api/bicicletas/create',
            body: aBici
        }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});*/