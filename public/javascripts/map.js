var map = L.map('main_map').setView([-12.054300, -77.044026], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/*L.marker([-8.993982, -78.619268]).addTo(map)
.bindPopup('Chimbote, Santa')
    .openPopup();

L.marker([-8.993982, -78.565473]).addTo(map)
.bindPopup('Nuevo Chimbote, Santa')
    .openPopup();*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);

        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
        
    }

})