var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code : Number,
    color : String,
    modelo : String,
    ubicacion : {
        type:[Number], index:{type:'2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code : code,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion
    });
}

bicicletaSchema.methods.toString = function(){
    return 'id: ' + this.id + "| color: " + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code : aCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code : aCode}, cb);
}


module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*var Bicicleta= function(id, color, modelo, ubicacion)
{
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + "| color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(pBici){
    Bicicleta.allBicis.push(pBici);
}

Bicicleta.findById = function(pBici){
    var findId= Bicicleta.allBicis.find(x=>x.id==pBici);
    if(findId)
        return findId;
    else
        throw new Error(`No existe una bicicleta con el Id: ${pBici}`);
}

Bicicleta.removeById = function(pBici)
{
    for(var i=0; Bicicleta.allBicis.length; i++)
    {
        if(Bicicleta.allBicis[i].id==pBici)
        {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'Rojo','Urbana', [-12.052233, -77.048586]);
var b = new Bicicleta(2, 'Blanca','Urbana', [-12.050817, -77.042803]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;*/